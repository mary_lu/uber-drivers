<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDummyWasteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::drop('pages');
        Schema::drop('posts');
        Schema::drop('categories');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $posts = new CreatePostsTable();
        $posts->up();

        $pages = new CreatePagesTable();
        $pages->up();

        $categories = new CreateCategoriesTable();
        $categories->up();
    }
}
