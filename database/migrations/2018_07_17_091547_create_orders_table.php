<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->nullable();

            $table->timestamp('pickup_datetime');
            $table->text('pickup_place');

            $table->string('contact_name');
            $table->string('contact_phone');
            $table->integer('pax'); // count of people

            $table->enum('status', ['NEW', 'ACCEPTED', 'DONE', "CANCELED"])->default('NEW');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
